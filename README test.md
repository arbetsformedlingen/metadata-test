# Arbetsförmedlingens öppna data:

Här finns Arbetsförmedlingens öppna data som datafiler. Denna filserver skördas av [Sveriges Dataportal](https://dataportal.se) varje dag.  
Utöver dessa öppna data tillhandahåller Arbetsförmedlingen också API:er med öppna datalicenser samt samverkans-API:er som kräver behörighet.  
Navigera bland dessa och läs mer på [JobTech Dev] (https://jobtechdev.se).


